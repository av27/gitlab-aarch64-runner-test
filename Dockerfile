# syntax=docker/dockerfile:1
FROM --platform=$TARGETPLATFORM python:3.12-slim-bullseye

ARG HELMFILE_VERSION="0.165.0"
ARG HELM_VERSION="3.15.1"
ARG SOPS_VERSION="3.8.1"

ARG user=runner
ARG group=runner
ARG uid=1000
ARG gid=1000
ARG OS=linux
# auto-populated by buildkit
ARG TARGETARCH
ENV ARCH=${TARGETARCH:-amd64}

ENV HOME=/home/${user}
RUN groupadd -g ${gid} ${group} \
    && useradd --home-dir $HOME --uid ${uid} --gid ${group} ${user}

ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:
ENV CURL="curl --retry 10 --max-time 30 --retry-all-errors -L"
# aws cli
# no need to purge pip cache as cache has been disabled.
RUN pip3 --no-cache-dir install awscli
# tools, docker
RUN apt-get -y update && apt -y install openssl wget jq unzip curl git gnupg lsb-release ca-certificates amazon-ecr-credential-helper \
  && mkdir -p /etc/apt/keyrings \
  && curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg \
  && echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null \
  && apt-get -y update \
  && apt-get -y install docker-ce docker-ce-cli \
  && apt-get -y clean \
  && rm -rf /var/lib/apt/lists/*;
# helm + plugins
RUN ${CURL} https://get.helm.sh/helm-v${HELM_VERSION}-${OS}-${ARCH}.tar.gz | tar -xzO ${OS}-${ARCH}/helm > /usr/local/bin/helm \
  && chmod +x /usr/local/bin/helm \
  && mkdir -p ${HOME}/.config/helm \
  && helm plugin install https://github.com/jkroepke/helm-secrets \
  && helm plugin install https://github.com/databus23/helm-diff \
  && helm plugin install https://github.com/aslafy-z/helm-git

# yq
RUN ${CURL} -o /usr/local/bin/yq https://github.com/mikefarah/yq/releases/latest/download/yq_${OS}_${ARCH}  && chmod +x /usr/local/bin/yq

# kubectl
RUN ${CURL} -O https://storage.googleapis.com/kubernetes-release/release/$(${CURL} -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/${OS}/${ARCH}/kubectl \
  && mv kubectl /usr/local/bin/kubectl \
  && chmod +x /usr/local/bin/kubectl

# Helmfile
WORKDIR /usr/local/bin
RUN ${CURL} https://github.com/helmfile/helmfile/releases/download/v${HELMFILE_VERSION}/helmfile_${HELMFILE_VERSION}_${OS}_${ARCH}.tar.gz -o helmfile.tar.gz && \
    tar --no-same-owner --no-same-permissions -xvzf helmfile.tar.gz && rm -f helmfile.tar.gz && chmod +x /usr/local/bin/helmfile

# SOPS
RUN ${CURL} https://github.com/getsops/sops/releases/download/v${SOPS_VERSION}/sops-v${SOPS_VERSION}.${OS}.${ARCH} -o /usr/local/bin/sops && \
    chmod +x /usr/local/bin/sops

RUN chown -R ${user}:${group} ${HOME} && \
    chmod -R 777 ${HOME}

# allow user to connect to docker socket
RUN usermod -aG docker ${user}

WORKDIR /home/${user}
USER ${user}

RUN ls -la /usr/local/bin/

ENTRYPOINT [ "/bin/bash" ]
